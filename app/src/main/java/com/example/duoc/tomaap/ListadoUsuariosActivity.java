package com.example.duoc.tomaap;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ListadoUsuariosActivity extends AppCompatActivity {
    private TextView tvPrueba;
    private Button btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_usuarios);
        //RECUPERAR INTENT
        Intent myIntent = getIntent();
        String usuario = myIntent.getStringExtra("nombreUsuario");
        //SALUDAR AL USUARIO
        tvPrueba = (TextView) findViewById(R.id.tvPRUEBA);
        tvPrueba.setText("Bienvenido:" + usuario);

        btnSalir = (Button) findViewById(R.id.btnSalir);

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
